import os
from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch_ros.actions import Node
from launch.substitutions import LaunchConfiguration
from launch.actions import IncludeLaunchDescription
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import ThisLaunchFileDir


def generate_launch_description():
    navigation2_slam_server_prefix = get_package_share_directory(
        'navigation2_slam_server')
    nav2_bringup_launch_file_dir = os.path.join(
        get_package_share_directory('nav2_bringup'), 'launch')

    cartographer_config_dir = LaunchConfiguration('cartographer_config_dir',
                                                  default=os.path.join(navigation2_slam_server_prefix, 'config'))
    configuration_basename = LaunchConfiguration(
        'configuration_basename', default='turtlebot3_lds_2d.lua')

    resolution = LaunchConfiguration('resolution', default='0.05')
    publish_period_sec = LaunchConfiguration(
        'publish_period_sec', default='1.0')

    return LaunchDescription([
        IncludeLaunchDescription(
            PythonLaunchDescriptionSource(
                [nav2_bringup_launch_file_dir, 'nav2_navigation_launch.py']),
        ),

        DeclareLaunchArgument(
            'cartographer_config_dir',
            default_value=cartographer_config_dir,
            description='Full path to config file to load'),
        DeclareLaunchArgument(
            'configuration_basename',
            default_value=configuration_basename,
            description='Name of lua file for cartographer'),
        Node(
            package='cartographer_ros',
            node_executable='cartographer_node',
            arguments=['-configuration_directory', cartographer_config_dir, '-configuration_basename', configuration_basename]),

        DeclareLaunchArgument(
            'resolution',
            default_value=resolution,
            description='Resolution of a grid cell in the published occupancy grid'),
        DeclareLaunchArgument(
            'publish_period_sec',
            default_value=publish_period_sec,
            description='OccupancyGrid publishing period'),
        Node(
            package='cartographer_ros',
            node_executable='occupancy_grid_node',
            arguments=['-resolution', resolution, '-publish_period_sec', publish_period_sec]),

        Node(
            package='navigation2_slam_server',
            node_executable='random_explorer'),
    ])
