// Copyright 2019 ROB768

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "rclcpp/rclcpp.hpp"

using namespace std::placeholders;
using namespace std::chrono_literals;
using PoseStampedMsg = geometry_msgs::msg::PoseStamped;

const std::string NODE_NAME = "random_explorer";
const float MAX_RANDOM_DISTANCE = 1;  // m, in both x and y

class RandomExplorer : public rclcpp::Node {
  /* Properties */
 private:
  rclcpp::Publisher<PoseStampedMsg>::SharedPtr pub_random_goal_;
  rclcpp::TimerBase::SharedPtr timer_;
  PoseStampedMsg previous_goal_;

  /* Methods */
 public:
  RandomExplorer();

 private:
  void publish_initial_pose();
  float uniform_random_offset(float max_offset);
};

RandomExplorer::RandomExplorer() : Node(NODE_NAME) {
  // Initialise goal pose
  previous_goal_.header.frame_id = "map";
  previous_goal_.pose.position.x = 0;
  previous_goal_.pose.position.y = 0;
  previous_goal_.pose.position.z = 0;
  previous_goal_.pose.orientation.x = 0;
  previous_goal_.pose.orientation.y = 0;
  previous_goal_.pose.orientation.z = 0;
  previous_goal_.pose.orientation.w = 1;

  pub_random_goal_ =
      this->create_publisher<PoseStampedMsg>("/move_base_simple/goal", 1);
  timer_ = this->create_wall_timer(
      10s, std::bind(&RandomExplorer::publish_initial_pose, this));
}

void RandomExplorer::publish_initial_pose() {
  // Modify goal with random offset from the previous goal
  previous_goal_.pose.position.x += uniform_random_offset(MAX_RANDOM_DISTANCE);
  previous_goal_.pose.position.y += uniform_random_offset(MAX_RANDOM_DISTANCE);

  // Update timestamp
  previous_goal_.header.stamp =
      (std_msgs::msg::Header::_stamp_type)rclcpp::Clock().now();

  // Publish the inital pose
  pub_random_goal_->publish(previous_goal_);
}

float RandomExplorer::uniform_random_offset(float max_offset) {
  return ((2 * max_offset) * float(rand()) / float(RAND_MAX)) - max_offset;
}

int main(int argc, char* argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<RandomExplorer>());
  rclcpp::shutdown();
  return 0;
}
